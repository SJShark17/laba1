package ru.zolotukhin.calculator_addition;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class StartActivity extends Activity {
    public EditText first_variable;
    public EditText second_variable;
    public Button button_addition;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        first_variable=(EditText)findViewById(R.id.first_variable);
        second_variable=(EditText)findViewById(R.id.second_variable);
        button_addition = (Button) findViewById(R.id.button_addition);
        data_transmission();
    }
    private boolean isValidate(){
        boolean flag=true;
        if(first_variable.getText().length()==0){
            first_variable.setError(getString(R.string.error1));
            flag=false;
        }
        if(second_variable.getText().length()==0){
            second_variable.setError(getString(R.string.error2));
            flag=false;
        }
        return flag;

    }
    public void data_transmission(){
        button_addition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isValidate()){
                Intent intent = new Intent(StartActivity.this, infoActivity.class);
                intent.putExtra("first_variable", first_variable.getText().toString());
                intent.putExtra("second_variable", second_variable.getText().toString());
                startActivity(intent);}
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}

