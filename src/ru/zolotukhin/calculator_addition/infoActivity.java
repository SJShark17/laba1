package ru.zolotukhin.calculator_addition;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;


public class infoActivity extends Activity {
    public TextView outPutInfo;
    public int first;
    public int second;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_output);
        outPutInfo=(TextView)findViewById(R.id.outPutInfo);
        int outVariable=addition();
        outPutInfo.setText( getIntent().getStringExtra("first_variable") + "+" + getIntent().getStringExtra("second_variable") + "=" + outVariable);
    }
    public int addition(){
       int first=Integer.valueOf( getIntent().getStringExtra("first_variable"));
        int second=Integer.valueOf(getIntent().getStringExtra("second_variable"));
        int result= first+second;
        return result;
    }
}